﻿using Service.Dto;

namespace Service.Interfaces
{
    public interface IShopService
    {
        ArticleDto GetById(int id);
        void OrderAndSellArticle(int id, int maxExpectedPrice, int buyerId);
    }
}