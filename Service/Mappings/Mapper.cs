﻿using Domain;
using Service.Dto;

namespace Service.Mappings
{
    public static class Mapper
    {
        /* 
         * Automapper should be used here. 
         * But, since external libraries were not used anywhere 
         * in this test application, so Automapper was not included.*/
        public static ArticleDto Map(Article article)
        {
            return new ArticleDto
            {
                ArticlePrice = article.ArticlePrice,
                BuyerUserId = article.BuyerUserId,
                Id = article.Id,
                IsSold = article.IsSold,
                NameOfArticle = article.NameOfArticle,
                SoldDate = article.SoldDate
            };
        }
    }
}