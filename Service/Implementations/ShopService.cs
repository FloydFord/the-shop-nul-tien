﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Service.Dto;
using Service.Interfaces;
using Service.Mappings;

namespace Service.Implementations
{
    public class ShopService : IShopService
    {
        private readonly DatabaseDriver _databaseDriver;
        private readonly Logger _logger;

        private readonly IEnumerable<ISupplier> _suppliers;

        public ShopService()
        {
            _databaseDriver = new DatabaseDriver();
            _logger = new Logger();
            _suppliers = new List<ISupplier>
            {
                new Supplier1(),
                new Supplier2(),
                new Supplier3()
            };
        }

        public void OrderAndSellArticle(int id, int maxExpectedPrice, int buyerId)
        {
            var article = OrderArticle(id, maxExpectedPrice);
            SellArticle(buyerId, article);
        }

        private void SellArticle(int buyerId, Article article)
        {
            if (article == null)
                throw new Exception("Could not order article");

            _logger.Debug($"Trying to sell article with id = {article.Id}.");

            article.Sell(buyerId);

            try
            {
                _databaseDriver.Save(article);
                _logger.Info($"Article with id = {article.Id} is sold.");
            }
            catch (Exception ex)
            {
                var errorMessage = $"Could not save article with id = {article.Id}.";
                _logger.Error(errorMessage);
                throw new Exception(errorMessage, ex);
            }
        }

        private Article OrderArticle(int id, int maxExpectedPrice)
        {
            return _suppliers.LastOrDefault(s =>
                        s.ArticleInInventory(id) &&
                        s.GetArticle(id).ArticlePrice > maxExpectedPrice)
                    ?.GetArticle(id);
        }

        public ArticleDto GetById(int id)
        {
            return Mapper.Map(_databaseDriver.GetById(id));
        }
    }

    //in memory implementation
}